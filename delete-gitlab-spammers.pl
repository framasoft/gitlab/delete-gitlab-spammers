#!/usr/bin/env perl
use strict;
use warnings;
use Mojo::Base -base;

use Date::Parse;
use GitLab::API::v4;
use Mojo::JSON qw(decode_json);
use Mojo::UserAgent;

my $config = decode_json(Mojo::File->new('config.json')->slurp) || {};

die "You have to set Gitlab URL in config.json!"            unless (defined($config->{gitlab_url}));
die "You have to set a Gitlab API token in config.json!"    unless (defined($config->{secret_token}));

my $api = GitLab::API::v4->new(
    url           => $config->{gitlab_url},
    private_token => $config->{secret_token}
);

$|++; # unbuffered output

say "Fetching all users";
my $users = $api->paginator('users');
my (@to_delete, @suspects);
my $time = time - 86400 * 7; # 7 days ago
while (my $user = $users->next()) {
    # Get users that never sign in and which account has been created more than 7 days ago
    if (!defined($user->{last_sign_in_at}) && str2time($user->{created_at}) < $time) {
        push @to_delete, $user;
    } elsif (($user->{bio} && $user->{bio} =~ m#escort|poker#) || ($user->{website_url} && $user->{website_url} =~ m#escort|poker#)) {
        push @suspects, $user;
    }
}

@to_delete = sort {$a->{username} <=> $b->{username}} @to_delete;
say sprintf("Users who never sign in (%d):", scalar(@to_delete)) if scalar(@to_delete);
for my $user (@to_delete) {
    say <<EOF;

Never signed in: $user->{username}
  created at: $user->{created_at}
  account page: $user->{web_url}
EOF
}

@suspects = sort {$a->{username} <=> $b->{username}} @suspects;
say sprintf("Users suspected of spam (%d):", scalar(@suspects)) if scalar(@suspects);
for my $user (@suspects) {
    say <<EOF;

Suspect: $user->{username}
  website: $user->{website_url}
  bio: $user->{bio}
EOF
    say "Do you want to delete that account? [y/N]";
    if (<> =~ m#^(yes|oui|y|o)$#) {
        $api->delete_user($user->{id});
    }
}

say sprintf("Users who never sign in (%d):", scalar(@to_delete)) if scalar(@to_delete);
say sprintf("Users suspected of spam (%d):", scalar(@suspects)) if scalar(@suspects);
